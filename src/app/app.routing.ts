/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule} from "@angular/router";


const appRoutes: Routes = [
	// {path: '', component: HomeComponent}, //Página Home, ruta inicial

	// {path: 'home', component: HomeComponent},
	// {path: 'login', component: LoginComponent},
	// {path: 'repuestos', component: RepuestosComponent},
	// {path: 'repuestos/:id', component: RepuestosComponent},

	// {path: '**', component: ErrorComponent} //Página para errores, cuando la ruta falla
]

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);